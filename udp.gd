extends Node

var IP_SERVER = "127.0.0.1"
var PORT_SERVER = 3663
var PORT_CLIENT = 3663
var socketUDP = PacketPeerUDP.new()

signal value_received(angle, motorPower)

export var desiredAngle = 0.0
export var angle = 0.0
export var motorPower = 0.0

func _ready():
	start_client()

func _process(delta):
	#if socketUDP.is_listening():
	#    socketUDP.set_dest_address(IP_SERVER, PORT_SERVER)
	#    var stg = "hi server!"
	#    var pac = stg.to_ascii()
	#    socketUDP.put_packet(pac)
	#    print("send!")
	#    cron_send = 3

	if socketUDP.get_available_packet_count() > 0:
		var array_bytes = socketUDP.get_packet()
		#printt("msg server: " + array_bytes.get_string_from_ascii())
		var dataReceived = array_bytes.get_string_from_ascii().split(", ", true, 0)
		var rawAngle = float(dataReceived[0])
		var rawDesiredAngle = float(dataReceived[1])
		#var rawAngle = float(array_bytes.get_string_from_ascii())
		angle = rawAngle * PI / 180.0;
		desiredAngle = rawDesiredAngle * PI / 180.0;
		#motorPower = float(array_bytes.get_string_from_ascii())
		emit_signal("values_received", angle, desiredAngle)

func start_client():
	if (socketUDP.listen(PORT_CLIENT, IP_SERVER) != OK):
		printt("Error listening on port: " + str(PORT_CLIENT) + " in server: " + IP_SERVER)
	else:
		printt("Listening on port: " + str(PORT_CLIENT) + " in server: " + IP_SERVER)

func _exit_tree():
	socketUDP.close()
