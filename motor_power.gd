extends Label


var udp

# Called when the node enters the scene tree for the first time.
func _ready():
	udp = get_node("/root/Spatial/UDP")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	text = "%.1f°" % udp.motorPower
