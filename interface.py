import serial
import socket

ser = serial.Serial('/dev/ttyACM0', 9600)  # open serial port
print(ser.name)         # check which port was really used

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def readData():
    buffer = ""
    while True:
        oneByte = ser.read(1)
        if oneByte == b"\r":    #method should returns bytes
            return buffer
        else:
            buffer += oneByte.decode("ascii")


while True:
    #data = ser.read_until(b',').decode('ascii').strip(',').strip()
    data = ser.read_until(b"\n").decode('ascii')
    print(data)
    udp_socket.sendto(bytes(data, 'ascii'), ("127.0.0.1", 3663))


ser.close()             # close port

