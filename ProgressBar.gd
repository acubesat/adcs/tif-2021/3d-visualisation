extends ProgressBar

var udp

var previousAngle = 0.0
var previousTime = 0
var calmdownTime = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	udp = get_node("/root/Spatial/UDP")



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	

func _on_UDP_value_received(angle):
	var time = OS.get_ticks_usec()
	
	if angle == previousAngle:
		calmdownTime += 1
		if calmdownTime <= 3:
			return
	else:
		calmdownTime = 0

	var derivative = (angle - previousAngle) / float(time - previousTime)
	value = abs(derivative * 2e7 / 2 / PI)
	
	previousAngle = angle
	previousTime = time
